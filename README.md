If you insist on beautiful, solid wood heirloom-quality furniture made with no toxic ingredients or parts, our kids furniture stores in Los Angeles is your best destination. Come in and see our LA kids furniture and other products for yourself — and don’t forget to bring the kids!

Address: 1801 W Verdugo Ave, Burbank, CA 91506, USA

Phone: 818-841-5544

Website: https://kidsonlyfurniture.com